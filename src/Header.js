/* jshint esversion: 6 */

import React from 'react';
import logo from './logo_transparent.png';
import './style/css/Header.css';

class Header extends React.Component {
    render() {
        return (
            
            <div>
              <header className="App-header">
                <img src={logo} className="App-logo" alt="logo"/>
              </header>
            </div>
        );
    }
}

export default Header;
