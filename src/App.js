/* jshint esversion: 6 */

import React from 'react';
import './style/css/App.css';
import Header from './Header';
import Weather from './Weather';

class App extends React.Component {
    render() {
        return (

            <div>
              <Header />
              
              <div className="row">
	        <div className="col s12 m6 push-m3">

                  <Weather />                  
	        </div>
              </div>
            </div>

        );
    }
}

export default App;
